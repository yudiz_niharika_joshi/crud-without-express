require("dotenv").config;
const {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  loginUser,
} = require("./controllers/userController");
const port = process.env.PORT || 3000;
const http = require("http");
//const { createSecureServer } = require("http2");
const server = http.createServer((req, res) => {
  //Getting data of all the users
  if (req.url === "/api/users" && req.method === "GET") {
    getUsers(req, res);
  }

  //Registering a new user
  else if (req.url === "/api/users/registeration" && req.method === "POST") {
    createUser(req, res);
    //res.end("Hello");
  }
  //   else if (req.url === "/api/users/login" && req.method === "GET") {
  //       const email = "../"
  //       const id = req.url.split("/")[3];
  //     loginUser(req, res, email, pass);
  //     //res.end("Hello");
  //}
  //Getting data of single user by id
  else if (req.url.match(/\/api\/users\/([0-9]+)/) && req.method === "GET") {
    const id = req.url.split("/")[3]; //api/user/id
    getUser(req, res, id);
  }

  //Updating a user
  else if (req.url.match(/\/api\/users\/([0-9]+)/) && req.method === "PUT") {
    const id = req.url.split("/")[3]; //api/user/id
    updateUser(req, res, id);
  }

  //Deleting a user
  //Updating a user
  else if (req.url.match(/\/api\/users\/([0-9]+)/) && req.method === "DELETE") {
    const id = req.url.split("/")[3]; //api/user/id
    deleteUser(req, res, id);
  }

  //None of them match
  else {
    res.writeHead(404, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ message: "route note found" }));
  }
});
server.listen(port, () => {
  console.log(`Server listening on ${port}`);
});
