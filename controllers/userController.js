//Controllers are used to deal with what particular route is doing and interact with model to send data

const User = require("../models/userModel");
const { getPostData } = require("../utils");
//Gets all user and route GET /api/users
async function getUsers(req, res) {
  try {
    const users = await User.findAll();
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify(users));
  } catch (error) {
    console.log(error);
  }
}

//Gets one user and route GET /api/:id
async function getUser(req, res, id) {
  try {
    const user = await User.findById(id);
    if (!user) {
      res.writeHead(404, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "User id doesnt exsist" }));
    } else {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(JSON.stringify(user));
    }
  } catch (error) {
    console.log(error);
  }
}

//Create user and route POST /api/users/registeration
async function createUser(req, res) {
  try {
    const body = await getPostData(req);

    const { name, email, pass } = JSON.parse(body);
    const user1 = {
      name,
      email,
      pass,
    };
    const newUser = await User.create(user1);
    res.writeHead(201, { "Content-Type": "application/json" });
    return res.end(JSON.stringify(newUser));
  } catch (error) {
    console.log(error);
  }
}

//Create user and route PUT /api/users/:id
async function updateUser(req, res, id) {
  try {
    const user = await User.findById(id);
    if (!user) {
      res.writeHead(404, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "User id doesnt exsist" }));
    } else {
      const body = await getPostData(req);
      const { name, email, pass } = JSON.parse(body);
      const updateuser1 = {
        name: name || user.name,
        email: email || user.email,
        pass: pass || user.pass,
      };
      const updUser = await User.update(id, updateuser1);
      res.writeHead(200, { "Content-Type": "application/json" });
      return res.end(JSON.stringify(updUser));
    }
  } catch (error) {
    console.log(error);
  }
}

async function loginUser(req, res, email, pass) {
  try {
    const user = await User.findByEmailPass(email, pass);
    //     if (!user) {
    //       res.writeHead(404, { "Content-Type": "application/json" });
    //       res.end(JSON.stringify({ message: "User doesnt exsist" }));
    //     } else {
    //       const body = await getPostData(req);
    //       const {  email, pass } = JSON.parse(body);
    //       const loginUser = {
    //         email:  user.email,
    //         pass:  user.pass,
    //       };
    //       const logUser = await User.login(email,pass, loginUser);
    //       res.writeHead(200, { "Content-Type": "application/json" });

    //       return res.end(JSON.stringify(logUser));
    //     }
    //   }
    if (!user) {
      res.writeHead(404, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "User id doesnt exsist" }));
    } else {
      const body = await getPostData(req);
      const { email, pass } = JSON.parse(body);
      const loginUser = {
        email: user.email,
        pass: user.pass,
      };
      const logUser = await User.login(email, pass, loginUser);
      res.writeHead(200, { "Content-Type": "application/json" });
      res.write(JSON.stringify({ message: "User Logged in" }));
      res.end(JSON.stringify(user));
    }
  } catch (error) {
    console.log(error);
  }
}

//Gets delete user and route DELETE /api/users/:id
async function deleteUser(req, res, id) {
  try {
    const user = await User.findById(id);
    if (!user) {
      res.writeHead(404, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "User id doesnt exsist" }));
    } else {
      await User.remove(id);
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: `User with id:${id} is removed` }));
    }
  } catch (error) {
    console.log(error);
  }
}
module.exports = {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  loginUser,
};
