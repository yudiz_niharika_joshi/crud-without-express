//Models are used for CRUD OPERATIONS dealin with data n db
const { v4: uuidv4 } = require("uuid");
//uuidv4();
let users = require("../data/users");
const { writeDataToFile } = require("../utils");
function findAll() {
  return new Promise((resolve, reject) => {
    resolve(users);
  });
}

function findById(id) {
  return new Promise((resolve, reject) => {
    const user = users.find((i) => i.id == id);
    resolve(user);
  });
}

function remove(id) {
  return new Promise((resolve, reject) => {
    users = users.filter((i) => i.id != id);
    writeDataToFile("./data/users.json", users);
    resolve();
  });
}

function create(user) {
  return new Promise((resolve, reject) => {
    const newUser = { id: uuidv4(), ...user };
    users.push(newUser);
    writeDataToFile("./data/users.json", users);

    resolve(newUser);
  });
}

function update(id, user) {
  return new Promise((resolve, reject) => {
    const index = users.findIndex((i) => i.id == id);
    users[index] = { id, ...user };
    writeDataToFile("./data/users.json", users);
    resolve(users[index]);
  });
}
function findByEmailPass(email, pass) {
  const user = users.find((e, p) => e.email == email && p.pass == pass);
  resolve(user);
}
function login(email, pass, user) {
  return new Promise((resolve, reject) => {
    const user = users.find((e, p) => e.email == email && p.pass == pass);
    resolve(user);
  });
}

module.exports = {
  findAll,
  findById,
  create,
  update,
  remove,
  findByEmailPass,
  login,
};
